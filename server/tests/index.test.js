'use strict';

const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');

describe('DynamoDB Mock Test', function () {
    let AWS;
    let scriptToTest;
    let batchGetFunc;
    let scanFunc;

    before(function () {
        batchGetFunc = sinon.stub();
        scanFunc = sinon.stub();

        AWS = {
            DynamoDB: {
                DocumentClient: sinon.stub().returns({
                    batchGet: batchGetFunc,
                    scan: scanFunc
                })
            }
        };

        scriptToTest = proxyquire('../index', {
            'aws-sdk': AWS
        });
    });

    it('Should scan using async/await and promise', async function () {

        let users = []

        const getUser = async (user) => {
            const result = {
                Responses: {
                    users: [{UserId: user.id, Age: user.age, Name: user.name}],
                },
            };
            batchGetFunc.withArgs(sinon.match.any).returns({
                promise: () => result
            });

            return  await scriptToTest.fetchSingleUser('segf876seg876');

        }

        getUser({id: 'segf876seg876', age: 33, name: 'Paul'}).then(res => {
            users = [...users, res]
            assert.equal(users[0].id, 'segf876seg876');
        })

        const data = {
            usersList: [
                {user: {UserId: "kjb456kbj346", Age: 36, Name: "John"}, responseDuration: 518},
                {user: {UserId: "segf876seg876", Age: 33, Name: "Paul"}, responseDuration: 561},
                {user: {UserId: "sg987sdg987sdg", Age: 29, Name: "Ringo"}, responseDuration: 539},
                {user: {UserId: "3456lhjl346df", Age: 34, Name: "George"}, responseDuration: 525}
            ],
            averageAge: 33
        }

        data.usersList.map((user, i) => {
            getUser({id: user.user.UserId, age: user.user.Age, name: user.user.Name}).then(res => {
                const ages = []
                users = [...users, res]
                ages.push(user.user.Age);
                const sum = ages.reduce((a, b) => a + b, 0);
                const averageAge = (sum / ages.length) || 0;

                assert.equal(res.age, user.user.Age);
                assert.equal(averageAge, data.usersList.averageAge);
                assert.equal(users.length, data.usersList.length);
            })
        })

    });

});