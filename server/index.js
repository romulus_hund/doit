const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
require('dotenv').config();

app.use(express.json());
const AWS = require('aws-sdk');

const accessKeyId = process.env.ACCESS_KEY_ID || '';
const secretAccessKey = process.env.SECRET_ACCESS_KEY || '';

let awsConfig = {
    'region': 'us-east-2',
    'accessKeyId': accessKeyId, 'secretAccessKey': secretAccessKey
};

const docClient = new AWS.DynamoDB.DocumentClient(awsConfig);

const measureASyncFunc = fn => {
    const start = Date.now();
    return fn.catch(() => {
    }).then(() => {
        const end = Date.now();
        return end - start;
    });
};

const fetchUsers = async () => {
    const params = {
        TableName: 'users',
    };
    try {
        const users = {
            usersList: [],
            averageAge: null
        };
        const usersIds = [];
        const ages = [];

        const res = await docClient.scan(params).promise();
        res.Items.map(user => {
            usersIds.push(user.UserId);
            ages.push(user.Age);
        });
        const sum = ages.reduce((a, b) => a + b, 0);
        users.averageAge = (sum / ages.length) || 0;

        console.log(usersIds);

        return Promise.all(usersIds.map(id => getUserInfo(id))).then(response => {
            users.usersList.push(response);
            return {
                usersList: [...users.usersList][0],
                averageAge: users.averageAge
            };

        });
    } catch (e) {
        console.log('users::fetch::error - ', e);
    }
};

const fetchSingleUser = async (userId) => {
    try {
        let queryParams = {RequestItems: {}};
        queryParams.RequestItems['users'] = {
            Keys: [{'UserId': userId}],
            ProjectionExpression: 'UserId,Age,#UserName',
            ExpressionAttributeNames: {'#UserName': 'Name'}
        };
        const res = await docClient.batchGet(queryParams).promise();
        return res.Responses.users[0];
    } catch (e) {
        console.log('users::fetch::error - ', e);
    }
};

const getUserInfo = async (userId) => {
    const user = await fetchSingleUser(userId);
    const responseDuration = await measureASyncFunc(fetchSingleUser(userId));
    return {user, responseDuration};
};

const users = fetchUsers();
users.then(data => console.log('---data: ', JSON.stringify(data)));

app.listen(port, () => {
    console.log('server run at port 3000');
});

module.exports = {
    fetchUsers,
    fetchSingleUser
};