
## Available Scripts

In the "client" directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

In the "server" directory, you can run:

### `yarn server`

Launches the node index.js file which interacts  Amazon
DynamoDB using SDK.<br />

### `yarn test`
Launches
 the unit test for DynamoDB SDK methods using chai and sinon

