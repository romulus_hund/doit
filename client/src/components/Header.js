import React from 'react';
import style from './Header.module.scss';
import {Menu} from 'antd';

const {SubMenu} = Menu;

const buildMenu = (items) => {
    return (
        items.map(item => {
                if (item.children) {
                    return (
                        <SubMenu key={item.id} title={item.name}>
                            {item.children && item.children.length > 0
                                ? buildMenu(item.children)
                                : null}
                        </SubMenu>
                    );

                } else {
                    return (
                        <Menu.Item  key={item.id}>
                            {item.name}
                        </Menu.Item>
                    );
                }
            }
        )

    );
};

const Header = ({menuData}) => {
    return (
        <div className={style['header-wrapper']}>
            <Menu mode="horizontal">
                {menuData && buildMenu(menuData)}
            </Menu>
        </div>
    );
};

export default Header;