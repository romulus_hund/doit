import React, {useEffect, useState} from 'react';
import Header from './components/Header';
import axios from 'axios';
import './App.scss';

function App() {

    const [menuData, setMenuData] = useState([])

    useEffect(()=> {

        async function getMenuConfig() {
            const res = await axios.get('http://localhost:3000/menu-config.json')

            if(res.data){
                setMenuData(res.data)
            }
        }
        // Execute the created function directly
        getMenuConfig();

    }, []);

    return (
        <div className="App">
            <Header menuData={menuData} />
        </div>
    );
}

export default App;
